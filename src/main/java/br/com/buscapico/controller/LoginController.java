package br.com.buscapico.controller;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Login;
import br.com.buscapico.repository.LoginRepository;

@RestController
@RequestMapping("logins")

public class LoginController {
	private static final Logger log = LoggerFactory.getLogger(LoginController.class);

	@Autowired
	private LoginRepository  repository;
	
	@GetMapping("autenticar")
	public Optional<Login> autenticarLogin(@PathVariable String id) {
		return repository.findById(Long.parseLong(id));
	}
}
