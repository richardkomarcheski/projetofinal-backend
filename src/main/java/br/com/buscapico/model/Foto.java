package br.com.buscapico.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "foto")
public class Foto {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String descricao;
	private String path;

	
	
	@OneToOne
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;

	public Foto() {
	}
	
	public Foto(String descricao, String path, Usuario usuario) {
	super();
	this.descricao = descricao;
	this.path = path;
	this.usuario = usuario;
	}
}
