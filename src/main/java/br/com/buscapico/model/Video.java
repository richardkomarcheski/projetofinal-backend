package br.com.buscapico.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "video")
public class Video {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String descricao;
	private String urlYoutube;
	
	@OneToOne
	@JoinColumn(name = "usuario_id")
	private Usuario usuario;

	public Video() {
	}
	
	public Video(String descricao, String urlYoutube, Usuario usuario) {
	super();
	this.descricao = descricao;
	this.urlYoutube = urlYoutube;
	this.usuario = usuario;
	}
}
