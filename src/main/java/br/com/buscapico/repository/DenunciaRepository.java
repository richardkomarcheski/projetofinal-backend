package br.com.buscapico.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.buscapico.model.Denuncia;

@Repository
public interface DenunciaRepository extends CrudRepository<Denuncia, Long>{

}
