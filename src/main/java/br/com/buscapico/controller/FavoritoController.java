package br.com.buscapico.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Favorito;
import br.com.buscapico.repository.FavoritoRepository;
@RestController
@RequestMapping("favoritoes")
public class FavoritoController {
	private static final Logger log = LoggerFactory.getLogger(FavoritoController.class);

	@Autowired
	private FavoritoRepository repository;

	
	@GetMapping("/listar")
	public List<Favorito> listarFavoritos() {
		log.info("listarPicos");

		return (List<Favorito>) repository.findAll();
	}
}
