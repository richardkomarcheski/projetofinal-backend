package br.com.buscapico.model;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import br.com.buscapico.security.Perfil;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Entity
@Table(name = "usuario")
public class Usuario {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String nome;
	
	private String email;
	
	@JsonProperty(access = Access.WRITE_ONLY)
	private String senha;
	
	private LocalDateTime dataNascimento;

	private boolean ativo;
	
	
	@JsonProperty(access = Access.WRITE_ONLY)
	@OneToMany(cascade = {CascadeType.ALL})
	@JoinColumn(name = "contato_id")
	private List<Usuario> contatos;


	@JsonProperty(access = Access.WRITE_ONLY)
	@ElementCollection(fetch=FetchType.EAGER)
	@CollectionTable(name="PERFIS")
	@Getter(AccessLevel.NONE)
	@Setter(AccessLevel.NONE)
	private Set<Integer>perfis = new HashSet<>();
	
	
	@JsonIgnore
	public Set<Perfil> getPerfis(){
		return perfis.stream().map(x -> Perfil.toEnum(x)).collect(Collectors.toSet());
	}
	
	
	public void AddPerfil(Perfil perfil) {
		perfis.add(perfil.getCod());
	}
	
	public Usuario() {
	}

	
	
	public Usuario(String nome, String email, String senha) {
		super();
		this.nome = nome;
		this.email= email;
		this.senha = senha;
		AddPerfil(Perfil.ADMIN);
		
	}
	
	public Usuario(String nome, String email, String senha, LocalDateTime dataNascimento ) {
		super();
		this.nome = nome;
		this.email= email;
		this.senha = senha;
		this.dataNascimento = dataNascimento;
		AddPerfil(Perfil.ADMIN);
	}
	
	
	public Usuario(String nome, String email, String senha, LocalDateTime dataNascimento, List<Usuario> contatos ) {
		super();
		this.nome = nome;
		this.email= email;
		this.senha = senha;
		this.dataNascimento = dataNascimento;
		this.contatos = contatos;
		AddPerfil(Perfil.ADMIN);
	}
	
	

	
}
