package br.com.buscapico;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.com.buscapico.model.Evento;
import br.com.buscapico.repository.EventoRepository;

@Component
@Order(value = 9)
public class EventoRunner implements CommandLineRunner {
	private static final Logger log = LoggerFactory.getLogger(EventoRunner.class);
	
	
	@Autowired
	private EventoRepository repository;
	
	@Override
	public void run(String... args) throws Exception {

		log.info("Inicializando a tabela de Evento");
		
		
		repository.save(new Evento("campeonato de skate", LocalDateTime.now())) ;
		repository.save(new Evento("campeonato de skate", LocalDateTime.now())) ;
		repository.save(new Evento("campeonato de skate", LocalDateTime.now())) ;
		repository.save(new Evento("campeonato de skate", LocalDateTime.now())) ;
		repository.save(new Evento("campeonato de skate", LocalDateTime.now())) ;
		repository.save(new Evento("campeonato de skate", LocalDateTime.now())) ;
		repository.save(new Evento("campeonato de skate", LocalDateTime.now())) ;
		repository.save(new Evento("campeonato de skate", LocalDateTime.now())) ;
		
		
		log.info("Finalizando");
	}
}

