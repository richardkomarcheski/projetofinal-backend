package br.com.buscapico.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Usuario;
import br.com.buscapico.repository.UsuarioRepository;
import br.com.buscapico.security.Perfil;
import br.com.buscapico.security.UserSS;
import br.com.buscapico.services.exception.AuthorizationException;

@RestController
@RequestMapping("usuarios")
public class CadastroController {

	

	@Autowired
	private UsuarioRepository repository;

	
	
	@GetMapping("/email")
	public ResponseEntity<Usuario> find(@RequestParam(value="value") String email) {
		Usuario obj = repository.findByEmail(email);
		
		obj.setSenha("");
		
		return ResponseEntity.ok().body(obj);
	}
	
	
	
	@GetMapping(value = "/autenticar",  consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> checarUsuario(@RequestBody Usuario body) {
		Usuario aux = new Usuario();
		
		UserSS user = UserService.authenticated();
		if (user == null || !user.hasRole(Perfil.ADMIN) && !body.getId().equals(user.getId())) {
			throw new AuthorizationException("Acesso negado");
		}
		
		aux = repository.findByEmail(body.getEmail());
	
			
			if(aux != null) {		
				if(body.getSenha().equals(aux.getSenha())) {
					return new ResponseEntity<String>("dados válidos",
							HttpStatus.INTERNAL_SERVER_ERROR);	
				}else {
				return new ResponseEntity<String>("dados inválidos",
						HttpStatus.INTERNAL_SERVER_ERROR);	
				}
			}
			else
			{
				return new ResponseEntity<String>("dados inválidos",
						HttpStatus.INTERNAL_SERVER_ERROR);	
			}
			
	}
			
		

	@PostMapping(value = "/cadastrar", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> cadastrarUsuario(@RequestBody Usuario body) {
		Usuario userAux;
		userAux = repository.findByEmail(body.getEmail());
				
		if (userAux != null) {
			return new ResponseEntity<String>("Email já cadastrado ",
					HttpStatus.INTERNAL_SERVER_ERROR);	
		}

		else {
			try {
				body.setSenha(passwordEncoder().encode(body.getSenha()));
				repository.save(body);
				return new ResponseEntity<String>("Inserido com suscesso!", HttpStatus.OK);

			} catch (Exception ex) {
				return new ResponseEntity<String>("Erro ao inserir: " + ex.getMessage(),
						HttpStatus.INTERNAL_SERVER_ERROR);
			}
		}
	}
	
	@Bean
	public PasswordEncoder passwordEncoder() {
	    return new BCryptPasswordEncoder();
	}
}
