package br.com.buscapico;

import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.com.buscapico.model.Comentario;
import br.com.buscapico.model.Foto;
import br.com.buscapico.model.Pico;
import br.com.buscapico.model.Sessao;
import br.com.buscapico.model.Usuario;
import br.com.buscapico.model.Video;
import br.com.buscapico.repository.PicoRepository;
import br.com.buscapico.repository.SessaoRepository;


@Component
@Order(value = 15)
public class SessaoRunner implements CommandLineRunner {
	private static final Logger log = LoggerFactory.getLogger(SessaoRunner.class);
	
	@Autowired
	private SessaoRepository repository;
	@Autowired
	private PicoRepository picoRepository;
	
	private  List <Foto> fotos;
	private  List <Usuario> participantes;
	private  List <Video> videos;
	private  List <Comentario> comentarios;


	@Override
	public void run(String... args) throws Exception {

		log.info("Inicializando a tabela de sessões");
		Iterable<Pico> picos = picoRepository.findAll();

		
		repository.save(new Sessao("rolê", LocalDateTime.now(), LocalDateTime.now(),picos.iterator().next(),fotos,
				participantes ,videos,comentarios));
		repository.save(new Sessao("sessao de fotos", LocalDateTime.now(), LocalDateTime.now(),picos.iterator().next(),fotos,
				participantes ,videos,comentarios));
		repository.save(new Sessao("amigos", LocalDateTime.now(), LocalDateTime.now(),picos.iterator().next(),fotos,
				participantes ,videos,comentarios));
		
	
			
		log.info("Finalizando");
	}
}
