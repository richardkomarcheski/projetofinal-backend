package br.com.buscapico.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

import lombok.Data;

@Data
@Entity
public class Sessao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@OneToOne
	@JoinColumn(name = "pico_id")
	private Pico pico;

	private LocalDateTime dataSessao;
	private LocalDateTime dataCadastro;
	private String nome;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "foto_sessao_id")
	private List<Foto> fotos;

	
	@OneToMany
	@JoinColumn(name = "participante_id")
	private List<Usuario> participantes;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "video_sessao_id")
	private List<Video> videos;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "comentario_sessao_id")
	private List<Comentario> comentarios;

	
	public Sessao() {}
	

	public Sessao(String nome, LocalDateTime dataSessao ,LocalDateTime dataCadastro, Pico pico,List<Foto> fotos,
			List<Usuario> participantes, List<Video> videos, List<Comentario> comentarios) {
		super();
		this.nome = nome;
		this.dataSessao = dataSessao;
		this.dataCadastro = dataCadastro;
		this.pico = pico;
		this.fotos = fotos;
		this.participantes = participantes;
		this.videos = videos;
		this.comentarios = comentarios;
	}
	
}
