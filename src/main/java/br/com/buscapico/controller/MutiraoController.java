package br.com.buscapico.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Mutirao;
import br.com.buscapico.repository.MutiraoRepository;

@RestController
@RequestMapping("mutiraos")

public class MutiraoController {

	private static final Logger log = LoggerFactory.getLogger(MutiraoController.class);

	@Autowired
	private MutiraoRepository repository;

	@GetMapping("/listar")
	public List<Mutirao> listarPicos() {
		log.info("listarMutiroes");

		return (List<Mutirao>) repository.findAll();
	}
	@GetMapping("/{id}")
	public Optional<Mutirao> detalheMutirao(@PathVariable String id) {
		return repository.findById(Long.parseLong(id));
	}
	
	@DeleteMapping("/{id}/excluir")
	public void deletarMutirao(@PathVariable String id) {
		repository.deleteById(Long.parseLong(id));

	}
	
	@PutMapping("/{id}/editar")
	public void editarAnuncio(@PathVariable String id ,@RequestBody Mutirao body) {
		Optional<Mutirao> c = repository.findById(Long.parseLong(id));

		if (c.isPresent()) {
			Mutirao mutirao = c.get();
			mutirao.setDataMutirao(body.getDataMutirao());
			repository.save(mutirao);
		}
	}
	
	@PostMapping(value = "/inserir", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> cadastrarMutirao(@RequestBody Mutirao body) {
		try {
			repository.save(body);

			return new ResponseEntity<String>("Inserido com suscesso!", HttpStatus.OK);
			
		} catch (Exception ex) {
			return new ResponseEntity<String>("Erro ao inserir: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
