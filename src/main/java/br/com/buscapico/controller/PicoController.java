package br.com.buscapico.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Comentario;
import br.com.buscapico.model.Foto;
import br.com.buscapico.model.Pico;
import br.com.buscapico.repository.ComentarioRepository;
import br.com.buscapico.repository.PicoRepository;

@RestController
@RequestMapping("picos")
public class PicoController {
	private static final Logger log = LoggerFactory.getLogger(PicoController.class);

	@Autowired
	private PicoRepository repository;

	@Autowired
	private ComentarioRepository comentarioRepository;

	@GetMapping("/listar")
	public List<Pico> listarPicos() {
		log.info("listarPicos");

		return (List<Pico>) repository.findAll();
	}

	@PostMapping(value = "/inserir", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> cadastrarPico(@RequestBody Pico body) {
		try {
			log.info("cadastrarPico" + body);
			repository.save(body);
			return new ResponseEntity<String>("Inserido com suscesso!", HttpStatus.OK);

		} catch (Exception ex) {
			return new ResponseEntity<String>("Erro ao inserir: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/{id}")
	public Optional<Pico> detalhePico(@PathVariable String id) {
		return repository.findById(Long.parseLong(id));
	}

//	@GetMapping("/{id}/fotos")
//	public List<Foto> listarFotos(@PathVariable String id) {
//		Optional<Pico> pico = repository.findById(Long.parseLong(id));
//		return pico.map(Pico::getFotos).orElse(null);
//	}

	@PostMapping(value = "/{id}/comentar", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> comentar(@RequestBody Pico body) {
		try {
			repository.save(body);
			return new ResponseEntity<String>("Inserido com suscesso!", HttpStatus.OK);

		} catch (Exception ex) {
			return new ResponseEntity<String>("Erro ao inserir: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/{id}/comentario/{idComentario}")
	public void deletarComentario(@PathVariable String idComentario) {
		comentarioRepository.deleteById(Long.parseLong(idComentario));

	}

	@PutMapping("/{id}/comentario/{idComentario}/editar")
	public void editarComentario(@PathVariable String idComentario, @RequestBody String texto) {
		Optional<Comentario> c = comentarioRepository.findById(Long.parseLong(idComentario));

		if (c.isPresent()) {
			Comentario comentario = c.get();
			comentario.setTexto(texto);
			comentarioRepository.save(comentario);
		}
	}
}
