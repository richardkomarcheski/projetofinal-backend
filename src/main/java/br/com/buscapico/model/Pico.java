package br.com.buscapico.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "pico")
public class Pico {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nome;
	private String descricao;
	private double entrada;
	private LocalDateTime dataCriacao;

	@OneToOne(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name = "endreco_id")
	private Endereco endereco;

	@OneToMany
	@JoinColumn(name = "tipo_id")
	private List<Tipo> tipos;

//	@OneToMany
//	@JoinColumn(name = "comentario_id")
//	private List<Comentario> comentarios;

	@OneToMany
	@JoinColumn(name = "denuncia_id")
	private List<Denuncia> denuncias;

	@OneToMany
	@JoinColumn(name = "checkin_id")
	private List<CheckIn> checkIns;

	@OneToMany
	@JoinColumn(name = "avaliacao_id")
	private List<Avaliacao> avaliacoes;

//	@OneToMany(cascade = {CascadeType.ALL})
//	@JoinColumn(name = "video_id")
//	private List<Video> videos;
//
//	@OneToMany(cascade = CascadeType.ALL, fetch=FetchType.EAGER)
//	@JoinColumn(name = "foto_id")
//	private List<Foto> fotos;

	@OneToOne
	@JoinColumn(name = "criador_id")
	private Usuario criador;

	public Pico() {
	}

	public Pico(String nome, String descricao, double entrada, LocalDateTime dataCriacao) {
		super();
		this.nome = nome;
		this.descricao = descricao;
		this.entrada = entrada;
		this.dataCriacao = dataCriacao;
	}

}
