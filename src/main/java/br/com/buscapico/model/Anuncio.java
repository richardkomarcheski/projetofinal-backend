package br.com.buscapico.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;

import lombok.Data;

@Data
@Entity
@Table(name = "anuncio")
public class Anuncio {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	private String titulo;

	private double preco;

	private String descricao;

	private LocalDateTime dataCadastro;

	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "foto_anuncio_id")
	private List<Foto> fotos;

	
	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "comentario_anuncio_id")
	private List<Comentario> comentarios;

	@OneToOne
	@JoinColumn(name = "anunciante_id")
	private Usuario anunciante;

//	@JsonProperty(access = Access.WRITE_ONLY)
//	@OneToMany(cascade = {CascadeType.ALL})
//	@JoinColumn(name = "categoria_id")
//	private CategoriaAnuncio categoria;

	public Anuncio() {

	}
	public Anuncio(String titulo, double preco, String descricao, LocalDateTime dataCadastro, List<Foto> fotos, List<Comentario> comentarios, Usuario anunciante) {
		super();
		this.titulo = titulo;
		this.preco = preco;
		this.descricao = descricao;
		this.dataCadastro = dataCadastro;
		this.fotos = fotos;
		this.comentarios = comentarios;
		this.anunciante = anunciante;
	}

}
