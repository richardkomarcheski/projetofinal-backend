package br.com.buscapico.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.buscapico.model.CheckIn;

@Repository
public interface CheckInRepository extends CrudRepository<CheckIn, Long> {

}
