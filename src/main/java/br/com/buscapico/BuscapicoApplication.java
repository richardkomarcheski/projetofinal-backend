package br.com.buscapico;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BuscapicoApplication {

	private static final Logger log = LoggerFactory.getLogger(BuscapicoApplication.class);

	public static void main(String[] args) {
		log.info("Iniciando a aplicação");
		SpringApplication.run(BuscapicoApplication.class, args);
	}

}
