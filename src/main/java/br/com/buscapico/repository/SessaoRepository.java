package br.com.buscapico.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.buscapico.model.Sessao;

@Repository
public interface SessaoRepository extends CrudRepository<Sessao, Long>{

}
