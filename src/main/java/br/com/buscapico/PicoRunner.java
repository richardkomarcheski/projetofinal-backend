package br.com.buscapico;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.com.buscapico.model.Pico;
import br.com.buscapico.repository.PicoRepository;

@Component
@Order(value = 8)
public class PicoRunner implements CommandLineRunner {
	
	private static final Logger log = LoggerFactory.getLogger(PicoRunner.class);
	
	@Autowired
	private PicoRepository repository;

	@Override
	public void run(String... args) throws Exception {

		log.info("Inicializando a tabela de picos");
		

		repository.save(new Pico("Jardim Ambiental", "Pico clássico com transições.", 0d, LocalDateTime.now())) ;
		repository.save(new Pico("pista do gaúcho", "Pico clássico com transições.", 0d, LocalDateTime.now())) ;
		repository.save(new Pico("pista do atlético", "Pico clássico com transições.", 0d, LocalDateTime.now())) ;
		repository.save(new Pico("pista do guabirotuba", "Pico clássico com transições.", 0d, LocalDateTime.now())) ;
		repository.save(new Pico("pista do paiol", "Pico clássico com transições.", 0d, LocalDateTime.now())) ;
		repository.save(new Pico("pista da drop dead", "Pico clássico com transições.", 0d, LocalDateTime.now())) ;
		repository.save(new Pico("pista do água verde", "Pico clássico com transições.", 0d, LocalDateTime.now())) ;
		
		log.info("Finalizando");
	}
}
