package br.com.buscapico.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Comentario;
import br.com.buscapico.model.Foto;
import br.com.buscapico.model.Pico;
import br.com.buscapico.model.Sessao;
import br.com.buscapico.model.Video;
import br.com.buscapico.repository.FotoRepository;
import br.com.buscapico.repository.PicoRepository;
import br.com.buscapico.repository.SessaoRepository;
import br.com.buscapico.repository.VideoRepository;
@RestController
@RequestMapping("sessaos")
public class SessaoController {
	private static final Logger log = LoggerFactory.getLogger(SessaoController.class);

	@Autowired
	private SessaoRepository repository;
	
	@Autowired
	private PicoRepository picoRepository;
	
	@Autowired
	private VideoRepository videoRepository;
	
	@Autowired
	private FotoRepository fotoRepository;
	
	private Optional<Pico> pico;
	
	private Sessao sessao;
	
	private Video video;

	@GetMapping("/listar")
	public List<Sessao> listarSessaos() {
		log.info("listarSessoes");

		return (List<Sessao>) repository.findAll();
	}
	
	@GetMapping("/{id}")
	public Optional<Sessao> detalheSessao(@PathVariable String id) {
		return repository.findById(Long.parseLong(id));
	}
	
	@DeleteMapping("/{id}/excluir")
	public void deletarSessao(@PathVariable String id) {
		repository.deleteById(Long.parseLong(id));

	}
	
	@PutMapping("/{id}/editar")
	public void editarSessao(@PathVariable String id ,@RequestBody Sessao body) {
		Optional<Sessao> c = repository.findById(Long.parseLong(id));

		if (c.isPresent()) {
			Sessao sessao = c.get();
			sessao.setDataSessao(body.getDataSessao());
			sessao.setNome(body.getNome());
			sessao.setPico(body.getPico());
			sessao.setParticipantes(body.getParticipantes());
			repository.save(sessao);
		}
	}
	@PutMapping("/{id}/video")
	public void inserirVideo(@PathVariable String id ,@RequestBody Video body) {
		Optional<Sessao> c = repository.findById(Long.parseLong(id));
		if (c.isPresent()) {
			Sessao sessao = c.get();
			List<Video> listVideos = sessao.getVideos();
			listVideos.add(body);
			sessao.setVideos(listVideos);
			repository.save(sessao);
		}
	}
	
	@PutMapping("/{id}/foto")
	public void inserirFoto(@PathVariable String id ,@RequestBody Foto body) {
		Optional<Sessao> c = repository.findById(Long.parseLong(id));
		if (c.isPresent()) {
			Sessao sessao = c.get();
			List<Foto> listaFotos = sessao.getFotos();
			listaFotos.add(body);
			for(int i = 0; i < listaFotos.size();i++) {
				fotoRepository.save(listaFotos.get(i));
			}
			sessao.setFotos(listaFotos);
			repository.save(sessao);
		}
	}
	
	@PutMapping("/{id}/comentario")
	public void inserirComentario(@PathVariable String id ,@RequestBody Comentario body) {
		Optional<Sessao> c = repository.findById(Long.parseLong(id));
		if (c.isPresent()) {
			Sessao sessao = c.get();
			List<Comentario> listaComentario = sessao.getComentarios();
			listaComentario.add(body);
			sessao.setComentarios(listaComentario);
			repository.save(sessao);
		}
	}
	
	@PostMapping(value = "/inserir", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> cadastrarSessao(@RequestBody Sessao body) {
		try {
			
			pico = picoRepository.findById(body.getPico().getId());
			
			repository.save(body);

			return new ResponseEntity<String>("Inserido com suscesso!", HttpStatus.OK);
			
		} catch (Exception ex) {
			return new ResponseEntity<String>("Erro ao inserir: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
