package br.com.buscapico.controller;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Anuncio;
import br.com.buscapico.model.Comentario;
import br.com.buscapico.model.Etapa;
import br.com.buscapico.model.Evento;
import br.com.buscapico.model.Foto;
import br.com.buscapico.model.Sessao;
import br.com.buscapico.model.Video;
import br.com.buscapico.repository.EtapaRepository;
import br.com.buscapico.repository.EventoRepository;
import br.com.buscapico.repository.FotoRepository;

@RestController
@RequestMapping("eventos")
public class EventoController {
	private static final Logger log = LoggerFactory.getLogger(EventoController.class);

	@Autowired
	private EventoRepository repository;

	@Autowired
	private FotoRepository fotoRepository;
	
	@Autowired
	private EtapaRepository etapaRepository;
	
	@GetMapping("/listar")
	public List<Evento> listarEventos() {
		log.info("listarEventos");

		return (List<Evento>) repository.findAll();
	}
	
	@GetMapping("/{id}")
	public Optional<Evento> detalheEvento(@PathVariable String id) {
		return repository.findById(Long.parseLong(id));
	}
	
	@GetMapping("/{id}/etapa")
	public Optional<Etapa> detalheEtapa(@PathVariable String id) {
		return etapaRepository.findById(Long.parseLong(id));
	}
	
	
	@DeleteMapping("/{id}/excluir")
	public void deletarEvento(@PathVariable String id) {
		repository.deleteById(Long.parseLong(id));

	}
	@DeleteMapping("/{id}/excluir/etapa")
	public void deletarEtapa(@PathVariable String id) {
		etapaRepository.deleteById(Long.parseLong(id));

	}
	
	@PutMapping("/{id}/etapa")
	public void inserirComentario(@PathVariable String id ,@RequestBody Etapa body) {
		Optional<Evento> c = repository.findById(Long.parseLong(id));
		if (c.isPresent()) {
			Evento evento = c.get();
			List<Etapa> listaEtapas = evento.getEtapas();
			listaEtapas.add(body);
			evento.setEtapas(listaEtapas);
			repository.save(evento);
		}
	}
	@PutMapping("/{id}/video")
	public void inserirVideo(@PathVariable String id ,@RequestBody Video body) {
		Optional<Evento> c = repository.findById(Long.parseLong(id));
		if (c.isPresent()) {
			Evento sessao = c.get();
			List<Video> listVideos = sessao.getVideos();
			listVideos.add(body);
			sessao.setVideos(listVideos);
			repository.save(sessao);
		}
	}
	
	@PutMapping("/{id}/foto")
	public void inserirFoto(@PathVariable String id ,@RequestBody Foto body) {
		Optional<Evento> c = repository.findById(Long.parseLong(id));
		if (c.isPresent()) {
			Evento evento = c.get();
			List<Foto> listaFotos = evento.getFotos();
			listaFotos.add(body);
			for(int i = 0; i < listaFotos.size();i++) {
				fotoRepository.save(listaFotos.get(i));
			}
			evento.setFotos(listaFotos);
			repository.save(evento);
		}
	}
	
	@PutMapping("/{id}/comentario")
	public void inserirComentario(@PathVariable String id ,@RequestBody Comentario body) {
		Optional<Evento> c = repository.findById(Long.parseLong(id));
		if (c.isPresent()) {
			Evento evento = c.get();
			List<Comentario> listaComentario = evento.getComentarios();
			listaComentario.add(body);
			evento.setComentarios(listaComentario);
			repository.save(evento);
		}
	}
	
	
	@PutMapping("/{id}/editar")
	public void editarEvento(@PathVariable String id ,@RequestBody Evento body) {
		Optional<Evento> c = repository.findById(Long.parseLong(id));

		if (c.isPresent()) {
			Evento evento = c.get();
			evento.setNome(body.getNome());			
			repository.save(evento);
		}
	}
	
	@PutMapping("/{id}/editar/etapa")
	public void editaEtapa(@PathVariable String id ,@RequestBody Etapa body) {
		Optional<Etapa> c = etapaRepository.findById(Long.parseLong(id));

		if (c.isPresent()) {
			Etapa etapa = c.get();
			etapa.setNome(body.getNome());		
			etapa.setData(body.getData());
			etapa.setPico(body.getPico());
			etapa.setParticipantes(body.getParticipantes());
			etapaRepository.save(etapa);
		}
	}
	
	@PostMapping(value = "/inserir", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> cadastrarEvento(@RequestBody Evento body) {
		try {
			repository.save(body);

			return new ResponseEntity<String>("Inserido com suscesso!", HttpStatus.OK);
			
		} catch (Exception ex) {
			return new ResponseEntity<String>("Erro ao inserir: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
