package br.com.buscapico.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.buscapico.model.Mutirao;

@Repository
public interface MutiraoRepository extends CrudRepository<Mutirao, Long> {

}
