package br.com.buscapico.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "etapa")
public class Etapa {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private LocalDateTime data;

	private String nome;
	
	@OneToMany
	@JoinColumn(name = "participante_evento_id")
	private List<Usuario> participantes;
	
	@OneToOne
	@JoinColumn(name = "pico_id")
	private Pico pico;

	public Etapa() {

	}
	
	public Etapa(LocalDateTime data, Pico pico, String nome,  List<Usuario> participantes) {
		super();
		this.data = data;
		this.pico = pico;
		this.nome = nome;
		this.participantes = participantes;
	}

}
