package br.com.buscapico.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.Data;

@Entity
@Data
public class Mutirao {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private LocalDateTime dataMutirao;

	private LocalDateTime dataCadastro;

//	@OneToMany
//	@JoinColumn(name = "foto_id")
//	private List<Foto> fotos;

//	@OneToMany(cascade = {CascadeType.ALL})
//	@JoinColumn(name = "video_id")
//	private List<Video> videos;

//	@OneToMany
//	@JoinColumn(name = "comentario_id")
//	private List<Comentario> comentarios;

	public Mutirao() {

	}
	public Mutirao(LocalDateTime dataMutirao) {
		super();
		this.dataMutirao = dataMutirao;
	}
	
}
