package br.com.buscapico;

import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.com.buscapico.model.Anuncio;
import br.com.buscapico.model.Comentario;
import br.com.buscapico.model.Foto;
import br.com.buscapico.model.Usuario;
import br.com.buscapico.repository.AnuncioRepository;
import br.com.buscapico.repository.UsuarioRepository;


@Component
@Order(value = 9)
public class AnuncioRunner implements CommandLineRunner {
	private static final Logger log = LoggerFactory.getLogger(AnuncioRunner.class);





	private  List <Foto> fotos;
	private  List <Comentario> comentarios;
	private Usuario usuario;


	@Autowired
	private AnuncioRepository repository;
	
	@Autowired
	private UsuarioRepository repositoryUser;

	@Override
	public void run(String... args) throws Exception {

		log.info("Inicializando a tabela de Anúncio");
		
		usuario = repositoryUser.findByEmail("richardkomarcheski@gmail.com");

		repository.save(new Anuncio("rodinha de skate", 100.00,"4 rodinhas spitfire", LocalDateTime.now(), fotos, comentarios, usuario)) ;
		repository.save(new Anuncio("shape", 200.00,"novo", LocalDateTime.now(), fotos, comentarios, usuario)) ;
		repository.save(new Anuncio("rolamentos", 50.00,"8 rolamentos novos", LocalDateTime.now(), fotos, comentarios, usuario));


		log.info("Finalizando");
	}
}
