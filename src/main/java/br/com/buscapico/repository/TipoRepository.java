package br.com.buscapico.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.buscapico.model.Tipo;

@Repository
public interface TipoRepository extends CrudRepository<Tipo, Long> {

}
