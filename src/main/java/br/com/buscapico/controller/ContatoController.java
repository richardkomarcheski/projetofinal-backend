package br.com.buscapico.controller;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.buscapico.model.Anuncio;
import br.com.buscapico.model.Usuario;
import br.com.buscapico.repository.UsuarioRepository;

@RestController
@RequestMapping("contatos")
public class ContatoController {

	
	@Autowired
	private UsuarioRepository repository;
	private List<Usuario>  contatos = new ArrayList<Usuario>();

	
	
	@DeleteMapping("/{email}/{id}/excluir")
	public void deletarContato(@PathVariable String email,@PathVariable("id") String id) {
		Usuario user = repository.findByEmail(email);
		contatos = user.getContatos();
		
		for(int  i = 0; i < contatos.size(); i++) {
			if(contatos.get(i).getId() == Long.parseLong(id) ) {
				contatos.remove(i);
			}
		}
		user.setContatos(contatos);
		repository.save(user);
	}
	
	@GetMapping("/detalhes/{email}/{id}")
	public Usuario detalheContato(@PathVariable("email") String email,@PathVariable("id") String id ) {
		Usuario obj = repository.findByEmail(email);
		 Usuario userAux = null;
		contatos = obj.getContatos();
		for(int  i = 0; i < contatos.size(); i++) {
			if(contatos.get(i).getId() == Long.parseLong(id)) {
			  userAux = contatos.get(i);
			}
		}
		userAux.setSenha("");		
		return userAux;
	}
	
	@GetMapping("/listar/email")
	public ResponseEntity<List<Usuario>> find(@RequestParam(value="value") String email) {
		Usuario obj = repository.findByEmail(email);
		contatos = obj.getContatos();
		return ResponseEntity.ok().body(contatos);
	}
	
	@PostMapping(value = "/{email}/inserir", consumes = "application/json", produces = "application/json")
	public ResponseEntity<String> cadastrarContato(@RequestBody Usuario body,@PathVariable("email") String email) {
		try {
			Usuario obj = repository.findByEmail(body.getEmail());
			boolean existe = false;
			Usuario aux = repository.findByEmail(email);
			contatos = aux.getContatos();
			for(int  i = 0; i < contatos.size(); i++) {
				if(contatos.get(i).getEmail() == body.getEmail()) {
				  existe = true;
				}
			}
			if(existe != true) {
				contatos.add(obj);
				aux.setContatos(contatos);
				repository.save(aux);
				return new ResponseEntity<String>("Inserido com suscesso!", HttpStatus.OK);
			}else {
				return new ResponseEntity<String>("Erro ao inserir, contato já cadastrado ", HttpStatus.INTERNAL_SERVER_ERROR);
			}
	
			
		} catch (Exception ex) {
			return new ResponseEntity<String>("Erro ao inserir: " + ex.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
