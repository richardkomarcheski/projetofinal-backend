package br.com.buscapico.model;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class Evento {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	private String nome;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "etapa_id")
	private List<Etapa> etapas;

	private LocalDateTime dataCadastro;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "foto_evento_id")
	private List<Foto> fotos;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "video_evento_id")
	private List<Video> videos;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "comentario_evento_id")
	private List<Comentario> comentarios;

	public Evento() {

	}
	public Evento(String nome, LocalDateTime dataCadastro) {
		super();
		this.nome = nome;
		this.dataCadastro = dataCadastro;
	}
	 
	public Evento(String nome, List<Etapa> etapas,LocalDateTime dataCadastro, List<Foto> fotos,List<Video> videos,  List<Comentario> comentarios )
	{
		super();
		this.nome = nome;
		this.etapas = etapas;
		this.dataCadastro = dataCadastro;
		this.fotos = fotos;
		this.videos = videos;
		this.comentarios = comentarios;
	}

}
