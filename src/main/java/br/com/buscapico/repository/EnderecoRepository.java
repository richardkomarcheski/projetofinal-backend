package br.com.buscapico.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.buscapico.model.Endereco;

@Repository
public interface EnderecoRepository extends CrudRepository<Endereco, Long>{

}
