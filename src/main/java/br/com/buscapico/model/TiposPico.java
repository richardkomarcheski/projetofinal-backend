package br.com.buscapico.model;

public enum TiposPico {
	VERTICAL(1), STREET(2), MINIRAMP(3), PICODERUA(4);

	private final int valor;

	TiposPico(int opcaoTipo) {
		this.valor = opcaoTipo;
	}

	public int getValor() {
		return valor;
	}
}
