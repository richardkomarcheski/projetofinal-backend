package br.com.buscapico;

import java.time.LocalDateTime;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.com.buscapico.model.Mutirao;
import br.com.buscapico.repository.MutiraoRepository;


@Component
@Order(value = 9)
public class MutiraoRunner implements CommandLineRunner {
private static final Logger log = LoggerFactory.getLogger(MutiraoRunner.class);
	
	@Autowired
	private MutiraoRepository repository;
	
	@Override
	public void run(String... args) throws Exception {

		log.info("Inicializando a tabela de Anúncio");
		
		
		//repository.save(new Mutirao(LocalDateTime.now())) ;
	
		
		log.info("Finalizando");
	}
}
