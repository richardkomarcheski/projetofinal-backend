package br.com.buscapico;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import br.com.buscapico.model.Usuario;
import br.com.buscapico.repository.UsuarioRepository;

@Component
@Order(value = 1)
public class UsuarioRunner implements CommandLineRunner {


	@Autowired
	private PasswordEncoder passwordEncoder;

	private static final Logger log = LoggerFactory.getLogger(UsuarioRunner.class);

	@Autowired
	private UsuarioRepository repository;

	private List <Usuario> listaContatos = new ArrayList<Usuario>();

	@Override
	public void run(String... args) throws Exception {

		log.info("Inicializando a tabela de usuarios");


		repository.save(new Usuario("Richard", "richardkomarcheski@gmail.com" , passwordEncoder.encode("12345678")));
		repository.save(new Usuario("Joao", "joao@gmail.com" ,passwordEncoder.encode("12345678")));
		repository.save(new Usuario("junior", "jose@gmail.com" , passwordEncoder.encode("12345678")));
		repository.save(new Usuario("José", "junior@gmail.com" , passwordEncoder.encode("12345678")));

		Usuario obj = repository.findByEmail("richardkomarcheski@gmail.com");
		Usuario objAux = repository.findByEmail("joao@gmail.com");
		Usuario objAux2 = repository.findByEmail("jose@gmail.com");
		listaContatos.add(objAux);
		listaContatos.add(objAux2);
		obj.setContatos(listaContatos);
		repository.save(obj);
		log.info("Finalizando");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
}
